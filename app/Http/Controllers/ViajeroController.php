<?php

namespace App\Http\Controllers;

use App\Viajero;
use Illuminate\Http\Request;

class ViajeroController extends Controller
{
    public function index()
    {
        $viajero = Viajero::all();
        return response()->json(['viajeros'=>$viajero]);
    } 

    /** Guarda un viajero nuevo
     * Parametros: Datos del viajero
     * @retorna \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        if($request)
        {
        	$viajero = new Viajero($request->all());
	    	$viajero->save();    
            return response()->json(['message' => 'Viajero Guardado']);
        }
    }
 
    /**
     * Muestra un Viajero especificado.
     *
     * parametro  int  $id
     * retorna \Illuminate\Http\Response
     */
    public function show($id)
    {
        $viajero = Viajero::find($id);
        return response()->json(['viajero' => $viajero]);
    }
 
    /**
     * Actualiza los datos de un viajero.
     *
     * parametros  \Illuminate\Http\Request  $request
     * parametros  int  $id
     * retorno \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request)
        {
            $viajero = Viajero::find($id);
            $viajero->cedula = $request->input('cedula');
            $viajero->nombre = $request->input('nombre');
            $viajero->direccion = $request->input('direccion');
            $viajero->telefono = $request->input('telefono');
            $viajero->save();
            return response()->json(['message' => 'Viajero Actualizado']);
        }
    }
 
    /**
     * Elimina el viajero con el id especifico.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $viajero = Viajero::find($id);
        $viajero->delete();
        return response()->json(['message' => 'Viajero Eliminado']);
    }

}
