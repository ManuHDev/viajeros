<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Viajero extends Model
{
    protected $table = "viajeros";

    protected $fillable =['cedula','nombre','direccion','telefono'];

    public function viajes(){
    	return $this->belongsToMany('App\Viaje','viajero_viaje');
    }
}
